Synapse
==============

Building
--------------
- **make gen** - generate the lexer and parser
- **make build** - build the assembler

Running
--------------
- **make run** - run the assembler, assemble all files in the samples folder
- **syn <files>** - run the assembler, assembling all *<files>*

![Alt text](instructions.png)
![Alt text](registers.png)
