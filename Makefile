# project name (generate executable with this name)
TARGET     = syn
CC         = gcc
# compiling flags here
CFLAGS   = -I. -c

LINKER   = gcc -o
# linking flags here
LFLAGS   = -I. -lm

# change these to set the proper directories where each files shoould be
SRCDIR   = src
OBJDIR   = obj
BINDIR   = bin

SRCS     := $(wildcard $(SRCDIR)/*.c)
SRCOBJS  := $(SRCS:$(SRCDIR)/%.c=$(OBJDIR)/%.o)
RM       = rm -rf
MKDIR    = mkdir -p

# Build main source
.PHONY: build
build: $(BINDIR)/$(TARGET)
$(BINDIR)/$(TARGET): $(SRCOBJS)
	@$(MKDIR) $(@D)
	@$(LINKER) $@ $(LFLAGS) $(SRCOBJS)
	@echo "Linking complete!"

$(SRCOBJS): $(OBJDIR)/%.o : $(SRCDIR)/%.c
	@$(MKDIR) $(@D)
	@$(CC) $(CFLAGS) $< -o $@
	@echo "Compiled "$<" successfully!"

.PHONY: run
run:
	@echo "Running" ./$(BINDIR)/$(TARGET) samples/*.asm
	@./$(BINDIR)/$(TARGET) samples/*.asm

# Auto-generate files
.PHONY: gen
gen: $(SRCDIR)/Synapse.tab.c $(SRCDIR)/Synapse.tab.h $(SRCDIR)/Synapse.yy.c

# Generate parser
$(SRCDIR)/Synapse.tab.c $(SRCDIR)/Synapse.tab.h: Synapse.y
	@bison -d -b $(SRCDIR)/Synapse Synapse.y
	@echo "Auto generated parser."

# Generate lexer
$(SRCDIR)/Synapse.yy.c: Synapse.l  $(SRCDIR)/Synapse.tab.h
	@flex -o $(SRCDIR)/Synapse.yy.c Synapse.l
	@echo "Auto generated lexer."

# Cleanup targets
.PHONY: clean
clean:
	@$(RM) $(OBJDIR)
	@echo "Cleanup complete!"

.PHONY: rm
rm: clean
	@$(RM) $(BINDIR)
	@$(RM) $(SRCDIR)/Synapse.tab.c
	@$(RM) $(SRCDIR)/Synapse.tab.h
	@$(RM) $(SRCDIR)/Synapse.yy.c
	@echo "Executables and auto-generated files removed!"
