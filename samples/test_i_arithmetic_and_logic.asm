	.text
# I Type Instruction Setup
addi t0 r0 -10
ori  t1 r0 17
addi t2 r0 2
ori  t3 r0 -7
addi t4 r0 -8
ori  t5 r0 617
# I Type Instruction Execution
andi s0 t0 -0xf    # -16
addi s1 t4 100     # 92
ori  s2 t5 17      # 633
slti s3 t0 -9      # 1
andi s4 t5 0x5D59F # 9
addi s5 t5 -1000   # -383
ori  s6 t0 1       # -9
slti s7 t5 619     # 1
loi  t6 0x1529     # -143780567
hii  t6 0xF76E
halt

	.data
