	.text
j main:

ode:
	addi sp sp -12
	swf fa0 sp 8
	swf fa1 sp 4
	lwf fv0 sp 8
	lof fv1 0x0000
	hif fv1 0x4130
	mulf fv0 fv0 fv1
	addf fv0 fv0 fa1
	swf fv0 sp 0
	addi sp sp 12
	jr ra
main:
	addi sp sp -44
	swf fs0 sp 40
	swf fs1 sp 36
	swf fs2 sp 32
	lof fv0 0
	hif fv0 0
	swf fv0 sp 28
	lof fv0 0
	hif fv0 0
	swf fv0 sp 24
	lof fv0 0xd70a
	hif fv0 0x3f63 # 0.89
	swf fv0 sp 20
	lof fv0 0xb717
	hif fv0 0x38d1 # 0.0001
	swf fv0 sp 16
	lof fv0 0
	hif fv0 0
	swf fv0 sp 12
	lwf fv0 sp 24
	swf fv0 sp 8
	lwf fv0 sp 12
	swf fv0 sp 4
	j LBB11:
LBB12:
	lwf fs0 sp 4
	lwf fs2 sp 16
	lwf fa0 sp 8
	addf fa1 fr0 fs0
	jal ode:
	mulf fv0 fs2 fv0
	addf fv0 fs0 fv0
	swf fv0 sp 4
	lwf fv0 sp 8
	lwf fv1 sp 16
	addf fv0 fv0 fv1
	swf fv0 sp 8
LBB11:
	lwf fv0 sp 8
	lwf fv1 sp 20
	sltf ft0 fv0 fv1
	beqf ft0 fr0 LBB13:
	j LBB12:
LBB13:
	lwf fv1 sp 4
	lwf fv0 sp 8
	# lof ft2 0x0000
	# hif ft2 0xc130
	# lof ft3 0x0000
	# hif ft3 0x4130
	# mulf fa0 fv0 ft2
	# addf fa0 fa0 ft2
	# exp  ft0 fv0
	# mulf ft0 ft0 ft3
	# addf fa0 fa0 ft0
	lwf fs2 sp 32
	lwf fs1 sp 36
	lwf fs0 sp 40
	addi sp sp 44
	halt

	.data
