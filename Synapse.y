/* The bison file for generating the synapse parser. */

%{
#include "Helper.h"
%}

%union
{
	uint32_t val;
	uint32_t num;
	char*	 label;
}


/* declare tokens */
%token <val> ROP IOP MOP BOP JOP ROPF IOPF MOPF BOPF SOP INTREG FPREG DIRECTIVE
%token <num> NUM
%token <label> LABEL
%token EOL

%type <val> program instruction label directive

%%

program:
 /* nothing */			     { $$ = 0;}
 | program directive EOL     { $$ = 0; }   		   /* assembler directive */
 | program instruction EOL   { $$ = 0; }   		   /* instruction */
 | program label EOL   		 { $$ = 0; } 		   /* label */
 | program EOL               { $$ = 0; }           /* blank line or a comment */
 | program error EOL         { yyerrok; $$ = 0; }  /* error in instruction */
 ;

instruction:
   ROP   INTREG INTREG INTREG { RRInstruction($1, $2, $3, $4); $$ = 0; }
 | ROP   INTREG               { RRInstruction($1, Reg_r0, $2, Reg_r0); $$ = 0; }
 | ROP        		          { RRInstruction($1, Reg_r0, Reg_r0, Reg_r0); $$ = 0; }
 | IOP   INTREG INTREG NUM    { RIInstruction($1, $2, $3, $4); $$ = 0; }
 | IOP   INTREG NUM           { RIInstruction($1, $2, Reg_r0, $3); $$ = 0; }
 | IOP   INTREG LABEL         { RLInstruction($1, $2, Reg_r0, $3); $$ = 0; }
 | MOP   INTREG INTREG NUM    { RIInstruction($1, $2, $3, $4); $$ = 0; }
 | BOP   INTREG INTREG LABEL  { RLInstruction($1, $2, $3, $4); $$ = 0; }
 | JOP   LABEL                { AInstruction($1, $2); $$ = 0; }
 | JOP   INTREG               { RRInstruction($1, Reg_r0, $2, Reg_r0); $$ = 0; }
 | ROPF  FPREG  FPREG  FPREG  { RRInstruction($1, $2, $3, $4); $$ = 0; }
 | IOPF  FPREG  NUM           { RIInstruction($1, $2, Reg_fr0, $3); $$ = 0; }
 | MOPF  FPREG  INTREG NUM    { RIInstruction($1, $2, $3, $4); $$ = 0; }
 | BOPF  FPREG  FPREG LABEL   { RLInstruction($1, $2, $3, $4); $$ = 0; }
 | SOP   INTREG INTREG INTREG { RRInstruction($1, $2, $3, $4); $$ = 0; }
 | SOP   INTREG INTREG        { RRInstruction($1, Reg_r0, $2, $3); $$ = 0; }
 | SOP   FPREG  FPREG 	      { RRInstruction($1, $2, $3, Reg_fr0); $$ = 0; }
 | SOP   FPREG  FPREG INTREG  { RRInstruction($1, $2, $3, $4); $$ = 0; }
 ;

label: LABEL         { AddLabel($1); $$ = 0; }

directive:
	  DIRECTIVE     { Directive($1, 0);   $$ = 0; }
	| DIRECTIVE NUM { Directive($1, $2); $$ = 0; }

%%
