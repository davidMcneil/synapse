/***************************************************************************//**
    \file Helper.c
    \brief Helper functions and defintions for the synapse parser.
*//****************************************************************************/
#include "Helper.h"

/*-----------------------LOCAL MACROS-----------------------------------------*/
/** The max number of instructions to assemble. */
#define MAX_NUM_INSTRUCTIONS       (8192u)

/** The max number of data to assemble. */
#define MAX_NUM_DATA               (8192u)

/** The max length of an output filename. */
#define MAX_OUTPUT_FILENAME_LENGTH (80u)

/** The max length of a label in characters. */
#define MAX_NUM_LABELS             (100u)

/** The return value of strcmp on equality. */
#define STRING_EQUALITY            (0u)

/** Number of characters in input file extension. */
#define LENGTH_FILE_EXTENSION      (4u)

/*-----------------------LOCAL DEFINITIONS------------------------------------*/
/** A label. */
typedef struct {
    Address_t address;
    char label[MAX_LABEL_LENGTH];
} Label_t;

/*-----------------------LOCAL CONSTANTS--------------------------------------*/
/** The file extension of the output file */
const static char _outputFileExtension[4] = "bin";

/*-----------------------LOCAL VARIABLES--------------------------------------*/
/** The instructin storage array. */
static Instruction_t _instructions[MAX_NUM_INSTRUCTIONS];

/** The data storage array. */
static UnsignedWord_t _data[MAX_NUM_DATA];

/** Labels. Note: This should really be implemented as a hash map. */
static Label_t _labels[MAX_NUM_LABELS];

/** The current label index. */
static uint16_t _labelIndex = 0;

/** The index of the assembled instructions into the instruction storage array. */
static uint64_t _instructionIndex = 0;


/** The index of the assembled data into the data storage array. */
static uint64_t _dataIndex = 0;

/*-----------------------LOCAL FUNCTION PROTOTYPES----------------------------*/
Address_t resolveLabel(char* label);
Word_t encodeInstruction(Instruction_t instruction, Address_t pc);
void outputInstructions(FILE* outputFile);

/***************************************************************************//**
    Add a directive .

    \param[in] directive - The opcode.
*//****************************************************************************/
void Directive(Directive_t directive, UnsignedWord_t data) {
    if (directive == Directive_Datum) {
        _data[_dataIndex++] = data;
    }
}

/***************************************************************************//**
    Add a register, register operand instruction to our instruction storage.

    \param[in] op - The opcode.
    \param[in] r0 - Reg operand 0.
    \param[in] r1 - Reg operand 1.
    \param[in] r2 - Reg operand 2.
*//****************************************************************************/
void RRInstruction(Op_t op, Reg_t r0, Reg_t r1, Reg_t r2) {
    /* Get a pointer to the next instruction */
    Instruction_t* instruction = _instructions + _instructionIndex;
    /** Simply label all instructions as R type. */
    instruction->type = InstructionType_R;
    instruction->op = op;
    instruction->r0 = r0;
    instruction->r1 = r1;
    instruction->r2 = r2;
    instruction->line = yylineno;
    instruction->immediate = 0;  /* There is no immeadiate */
    instruction->address = 0; /* There is no address */
    strcpy(instruction->label, ""); /* There is no label */
    _instructionIndex++;
}

/***************************************************************************//**
    Add a register, immeadiate operand instruction to our insturction storage.

    \param[in] op - The opcode.
    \param[in] r0 - Reg operand 0.
    \param[in] r1 - Reg operand 1.
    \param[in] immediate - Immediate operand.
*//****************************************************************************/
void RIInstruction(Op_t op, Reg_t r0, Reg_t r1, Immediate_t immediate) {
    /* Get a pointer to the next instruction */
    Instruction_t* instruction = _instructions + _instructionIndex;
    /** Simply label all instructions as I type. */
    instruction->type = InstructionType_I;
    instruction->op = op;
    instruction->r0 = r0;
    instruction->r1 = r1;
    instruction->r2 = Reg_r0;  /* There is no rt */
    instruction->immediate = immediate;
    instruction->address = 0; /* There is no address */
    strcpy(instruction->label, ""); /* There is no label */
    instruction->line = yylineno;
    _instructionIndex++;
}

/***************************************************************************//**
    Add a register, label operand instruction to our insturction storage.

    \param[in] op - The opcode.
    \param[in] r0 - Reg operand 0.
    \param[in] r1 - Reg operand 1.
    \param[in] label - Label operand.
*//****************************************************************************/
void RLInstruction(Op_t op, Reg_t r0, Reg_t r1, char* label) {
    /* Get a pointer to the next instruction */
    Instruction_t* instruction = _instructions + _instructionIndex;
    if (op == Op_beq || op == Op_bne || op == Op_beqf || op == Op_bnef) {
        instruction->type = InstructionType_B;
    } else {
        /** Simply label all other instructions as I type. */
        instruction->type = InstructionType_I;
    }
    instruction->op = op;
    instruction->r0 = r0;
    instruction->r1 = r1;
    instruction->r2 = Reg_r0;  /* There is no rt */
    instruction->immediate = 0; /* There is no immediate */
    instruction->address = 0; /* There is no address */
    strcpy(instruction->label, label);
    instruction->line = yylineno;
    _instructionIndex++;
}

/***************************************************************************//**
    Add a address operand instruction to our instruction storage.

    \param[in] op - The opcode.
    \param[in] label - A label representing an absolute address.
*//****************************************************************************/
void AInstruction(Op_t op, char* label) {
    /* Get a pointer to the next instruction */
    Instruction_t* instruction = _instructions + _instructionIndex;
    instruction->type = InstructionType_J;
    instruction->op = op;
    instruction->r0 = Reg_r0; /* There is no r0 */
    instruction->r1 = Reg_r0; /* There is no r1 */
    instruction->r2 = Reg_r0; /* There is no r2 */
    instruction->immediate = 0; /* There is no immediate */
    instruction->address = 0; /* There is no address */
    strcpy(instruction->label, label);
    instruction->line = yylineno;
    _instructionIndex++;
}

/***************************************************************************//**
    Generate the instruction word for an instruction.

    \param[in] intsruction - The instruction.
    \param[in] pc - The word aligned address of the instruction.

    \return The encoded instruction word.
*//****************************************************************************/
Word_t encodeInstruction(Instruction_t instruction, Address_t pc) {
    Word_t result;
    result.u = 0;
    /** Resolve label if needed */
    if (strlen(instruction.label) > 0) {
        switch (instruction.type) {
            case InstructionType_I:
                if (instruction.op == Op_hii) {
                    /** Use high bits of word. */
                    instruction.immediate = (resolveLabel(instruction.label) >> 16);
                } else {
                    /** Load low bits of word. */
                    instruction.immediate = resolveLabel(instruction.label);
                }
                break;
            case InstructionType_B:
                instruction.immediate = resolveLabel(instruction.label) - (pc * 4);
                break;
            case InstructionType_J:
                instruction.address = resolveLabel(instruction.label);
                break;
            case InstructionType_R:
            default:
                assert(false);
        }
    }
    switch (instruction.type) {
        case InstructionType_R:
            result.u = (result.u |
                       (instruction.op << (RD_LENGTH + RS_LENGTH + RT_LENGTH + R_UNUSED_LENGTH)) |
                       (instruction.r1 << (RS_LENGTH + RT_LENGTH + R_UNUSED_LENGTH)) |
                       (instruction.r2 << (RT_LENGTH + R_UNUSED_LENGTH)) |
                        instruction.r0 << R_UNUSED_LENGTH);
            break;
        case InstructionType_I:
            result.u = (result.u |
                       (instruction.op << (RD_LENGTH + RS_LENGTH + IMMEDIATE_LENGTH)) |
                       (instruction.r1 << (RS_LENGTH + IMMEDIATE_LENGTH)) |
                       (instruction.r0 << (IMMEDIATE_LENGTH)) |
                        instruction.immediate);
            break;
        case InstructionType_B:
            result.u = (result.u |
                       (instruction.op << (RD_LENGTH + RS_LENGTH + IMMEDIATE_LENGTH)) |
                       (instruction.r0 << (RS_LENGTH + IMMEDIATE_LENGTH)) |
                       (instruction.r1 << (IMMEDIATE_LENGTH)) |
                        instruction.immediate);
            break;
        case InstructionType_J:
            result.u = (result.u |
                       (instruction.op << ADDRESS_LENGTH) |
                        instruction.address);
            break;
        default:
            assert(false);
    }
    return result;
}

/***************************************************************************//**
    Output the instructions to outputFile.

    \param[in] outputFile - The output file to write to.
*//****************************************************************************/
void outputInstructions(FILE* outputFile) {
    uint64_t i;
    Word_t encodedInstruction;
    for (i = 0; i < _instructionIndex; i++) {
        encodedInstruction = encodeInstruction(_instructions[i], i);
        fwrite(&encodedInstruction, sizeof encodedInstruction, 1, outputFile);
    }
}

/***************************************************************************//**
    Output the data to outputFile.

    \param[in] outputFile - The output file to write to.
*//****************************************************************************/
void outputData(FILE* outputFile) {
    uint64_t i;
    for (i = 0; i < _dataIndex; i++) {
        fwrite(&_data[i], sizeof _data[i], 1, outputFile);
    }
}

/***************************************************************************//**
    Add a new label.

    \param[in] label - The string represnation of the new label.
*//****************************************************************************/
void AddLabel(char* label) {
    Label_t l = {
        .address = (_instructionIndex + _dataIndex),
    };
    strcpy(l.label, label);
    _labels[_labelIndex++] = l;
}

/***************************************************************************//**
    Resolve the address of a label.

    \param[in] label - The label to get the address for.

    \return The address.
*//****************************************************************************/
Address_t resolveLabel(char* label) {
    int i;
    for (i = 0; i < _labelIndex; i++) {
        if (strcmp(label, _labels[i].label) == STRING_EQUALITY) {
            return (_labels[i].address * 4);
        }
    }
    printf("Unresolved label '%s'.\n", label);
}

/***************************************************************************//**
    The parser error function. Called when an unknown token is found.
*//****************************************************************************/
void yyerror(void) {
    static int lastLineError = -1; /** Initialize it to a value it will never be. */
    /* Only print an error message if the error is on a new line */
    if (yylineno != lastLineError)
    {
        fprintf(stderr, "Line %d: Invalid instruction.\n", yylineno);
        lastLineError = yylineno;
    }
}

/***************************************************************************//**
    The main program.
*//****************************************************************************/
int main(int argc, char **argv) {
    int i;
    FILE* InputFile;
    FILE* outputFile;
    char outputFileName[MAX_OUTPUT_FILENAME_LENGTH];

    for(i = 1; i < argc; i++)
    {
        // printf("Assembling: %s\n", argv[i]);
        InputFile = fopen(argv[i], "r");
        if(!InputFile)
        {
            perror(argv[i]);
            return (1);
        }

        /* Name the output file with the same base name as the input file */
        strcpy(outputFileName, argv[i]);
        /* Remove the input file extension */
        outputFileName[strlen(outputFileName) - LENGTH_FILE_EXTENSION] = '\0';
        sprintf(outputFileName, "%s.%s", outputFileName, _outputFileExtension);
        outputFile = fopen(outputFileName, "wb");

        /* Restart counters and label index for next file. */
        yyrestart(InputFile);
        _labelIndex = 0;
        _instructionIndex = 0;
        _dataIndex = 0;
        yylineno = 1;

        /* Parse the file and write out the binary machine code */
        yyparse();
        fclose(InputFile);
        outputInstructions(outputFile);
        outputData(outputFile);
    }
    return 0;
}
