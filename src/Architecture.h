/***************************************************************************//**
    \file Architecture.h
    \brief Specifies a simple RISC architecture.

    This file specifies the general architecture and ISA of a simple RISC
    processor. The purpose of this file is to express the architecture
    parameters and the ISAs parameters so these values can be easily shared
    between the simulator and the assembler.
*//****************************************************************************/
#ifndef __ARCHITECTURE_H__
#define __ARCHITECTURE_H__

#include <stdint.h>

#ifdef __cplusplus
namespace Arch {
#endif

/*-----------------------PUBLIC MACROS-----------------------------------------*/
/** Word size in bits. */
#define WORD_LENGTH                (32u)

/** The number of integer registers in the register file. */
#define NUM_INT_REGISTERS          (32u)

/** The number of floating point registers in the register file. */
#define NUM_FP_REGISTERS           (32u)

/** The number of instructions. */
#define NUM_INSTRUCTIONS           (43u)

/** The length of the instruction opcode in bits. */
#define OP_LENGTH                  (6u)

/** The length of the instruction RD field in bits. */
#define RD_LENGTH                  (5u)

/** The length of the instruction RS field in bits. */
#define RS_LENGTH                  (5u)

/** The length of the instruction RT feild in bits. */
#define RT_LENGTH                  (5u)

/** The length of an instruction immediate in bits. */
#define IMMEDIATE_LENGTH           (16u)

/** The length of an instruction address in bits. */
#define ADDRESS_LENGTH             (WORD_LENGTH - OP_LENGTH)

/** The length of the unused bits in an R type instruction. */
#define R_UNUSED_LENGTH            (WORD_LENGTH - OP_LENGTH - RD_LENGTH - RS_LENGTH - RT_LENGTH)

/*-----------------------PUBLIC DEFINITIONS------------------------------------*/
/** Unsigned word type. */
typedef uint32_t UnsignedWord_t;

/** Signed word type. */
typedef int32_t SignedWord_t;

/** Float word type */
typedef float FloatWord_t;

/** In this architecture a word is 32 bits this union allows us to access these
    bits as different types. */
typedef union Word {
   FloatWord_t    f;
   SignedWord_t   s;
   UnsignedWord_t u;
} Word_t;

/** The immediate type for this architecture. */
typedef uint16_t Immediate_t;

/** The address type for this architecture. */
typedef uint32_t Address_t;

/** Assembler directives. */
typedef enum Directive {
    Directive_Text,  /** Indicates start of test section. */
    Directive_Data,  /** Indicates start of data section. */
    Directive_Datum, /** Indicates a single piece of data. */
} Directive_t;

/** The instruction types in the ISA. */
typedef enum InstructionType {
    InstructionType_R,
    InstructionType_I,
    InstructionType_M,
    InstructionType_B,
    InstructionType_J,
    InstructionType_FR,
    InstructionType_FI,
    InstructionType_FB,
    InstructionType_FM,
    InstructionType_S,
} InstructionType_t;

/** The opcodes in the ISA. */
typedef enum Op {
    Op_nop   = 0,
    Op_add   = 1,
    Op_sub   = 2,
    Op_mul   = 3,
    Op_div   = 4,
    Op_and   = 5,
    Op_or    = 6,
    Op_xor   = 7,
    Op_shl   = 8,
    Op_shr   = 9,
    Op_slt   = 10,
    Op_addi  = 11,
    Op_andi  = 12,
    Op_ori   = 13,
    Op_slti  = 14,
    Op_loi   = 15,
    Op_hii   = 16,
    Op_lw    = 17,
    Op_sw    = 18,
    Op_beq   = 19,
    Op_bne   = 20,
    Op_j     = 21,
    Op_jal   = 22,
    Op_jr    = 23,
    Op_jrl   = 24,
    Op_addf  = 25,
    Op_subf  = 26,
    Op_mulf  = 27,
    Op_divf  = 28,
    Op_sltf  = 29,
    Op_lof   = 30,
    Op_hif   = 31,
    Op_lwf   = 32,
    Op_swf   = 33,
    Op_beqf  = 34,
    Op_bnef  = 35,
    Op_mod   = 36,
    Op_crv   = 37,
    Op_exp   = 38,
    Op_mac   = 39,
    Op_cas   = 40,
    Op_brk   = 41,
    Op_halt  = (NUM_INSTRUCTIONS - 1),
} Op_t;

/** The registers in each register file. There are two register files
    one for integer values and one for floating point values. */
typedef enum Reg {
    /* Integer registers. */
    Reg_r0 = 0,   /** Hard-wired to 0. */
    Reg_at = 1,   /** Reserved for pseudo-instructions. */
    Reg_v0 = 2,   /** Return values from functions. */
    Reg_v1 = 3,
    Reg_a0 = 4,   /** Arguments to functions - not preserved by subprograms. */
    Reg_a1 = 5,
    Reg_a2 = 6,
    Reg_a3 = 7,
    Reg_t0 = 8,   /** Temporary data, not preserved by subprograms. */
    Reg_t1 = 9,
    Reg_t2 = 10,
    Reg_t3 = 11,
    Reg_t4 = 12,
    Reg_t5 = 13,
    Reg_t6 = 14,
    Reg_t7 = 15,
    Reg_t8 = 16,
    Reg_t9 = 17,
    Reg_s0 = 18,  /** Saved registers, preserved by subprograms. */
    Reg_s1 = 19,
    Reg_s2 = 20,
    Reg_s3 = 21,
    Reg_s4 = 22,
    Reg_s5 = 23,
    Reg_s6 = 24,
    Reg_s7 = 25,
    Reg_k0 = 26,  /** Reserved for kernel. Do not use.. */
    Reg_k1 = 27,
    Reg_gp = 28,  /** Global Area Pointer (base of global data segment.) */
    Reg_sp = 29,  /** Stack Pointer. */
    Reg_fp = 30,  /** Frame Pointer. */
    Reg_ra = 31,  /** Return Address. */
    /* Floating point registers. */
    Reg_fr0   = 0, /* Hardwired to zero */
    Reg_fat   = 1, /** Reserved for pseudo-instructions. */
    Reg_fv0   = 2, /** Return values from functions. */
    Reg_fv1   = 3,
    Reg_fa0   = 4, /** Arguments to functions - not preserved by subprograms. */
    Reg_fa1   = 5,
    Reg_fa2   = 6,
    Reg_fa3   = 7,
    Reg_ft0   = 8, /** Temporary data, not preserved by subprograms. */
    Reg_ft1   = 9,
    Reg_ft2   = 10,
    Reg_ft3   = 11,
    Reg_ft4   = 12,
    Reg_ft5   = 13,
    Reg_ft6   = 14,
    Reg_ft7   = 15,
    Reg_ft8   = 16,
    Reg_ft9   = 17,
    Reg_ft10  = 18,
    Reg_ft11  = 19,
    Reg_fs0   = 20, /** Saved registers, preserved by subprograms. */
    Reg_fs1   = 21,
    Reg_fs2   = 22,
    Reg_fs3   = 23,
    Reg_fs4   = 24,
    Reg_fs5   = 25,
    Reg_fs6   = 26,
    Reg_fs7   = 27,
    Reg_fs8   = 28,
    Reg_fs9   = 29,
    Reg_fs10  = 30,
    Reg_fs11  = 31,
} Reg_t;

/*-----------------------PUBLIC CONSTANTS--------------------------------------*/
/*-----------------------PUBLIC VARIABLES--------------------------------------*/
/*-----------------------PUBLIC FUNCTION PROTOTYPES----------------------------*/

#ifdef __cplusplus
}
#endif

#endif /* __ARCHITECTURE_H__ */
