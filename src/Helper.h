/***************************************************************************//**
    \file Helper.h
    \brief Helper functions and defintions for the synapse parser.
*//****************************************************************************/
#ifndef __HELPER_H__
#define __HELPER_H__

#include <assert.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include "Architecture.h"

/*-----------------------PUBLIC MACROS-----------------------------------------*/
/** The max length of a label in characters. */
#define MAX_LABEL_LENGTH           (32u)

/*-----------------------PUBLIC DEFINITIONS------------------------------------*/
/** An instruction. All fields are not used by all instruction instances. */
typedef struct Instruction {
    InstructionType_t type;
    Op_t op;
    Reg_t r0;
    Reg_t r1;
    Reg_t r2;
    Immediate_t immediate;
    Address_t address;
    /** The label representing the immediate or address. Only used in assmbling */
    char label[MAX_LABEL_LENGTH];
    /** The line number of the instruction. Only used in assmbling */
    uint64_t line;
} Instruction_t;

/*-----------------------PUBLIC CONSTANTS--------------------------------------*/
/* The default value for an instruction. */
static const Instruction_t Architecture_DefaultInstruction = {
    .type = InstructionType_R,
    .op = Op_nop,
    .r0 = Reg_r0,
    .r1 = Reg_r0,
    .r2 = Reg_r0,
    .immediate = 0,
    .address = 0,
};

/*-----------------------PUBLIC VARIABLES--------------------------------------*/
extern int yylineno;

/*-----------------------PUBLIC FUNCTION PROTOTYPES----------------------------*/
extern void Directive(Directive_t directive, UnsignedWord_t data);
extern void RRInstruction(Op_t op, Reg_t r0, Reg_t r1, Reg_t r2);
extern void RIInstruction(Op_t op, Reg_t r0, Reg_t r1, Immediate_t immediate);
extern void RLInstruction(Op_t op, Reg_t r0, Reg_t r1, char* label);
extern void AInstruction(Op_t op, char* label);
extern void AddLabel(char* label);

#endif /* __HELPER_H__ */
