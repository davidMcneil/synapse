/* The flex file for generating the synapse lexical analyzer. */

%option noyywrap nodefault yylineno

%{
#include "Architecture.h"
#include "Synapse.tab.h"
%}

%%
 /* Opcodes. */
^[ \t]?nop   { yylval.val = Op_nop;  return ROP; }
^[ \t]?add   { yylval.val = Op_add;  return ROP; }
^[ \t]?sub   { yylval.val = Op_sub;  return ROP; }
^[ \t]?mul   { yylval.val = Op_mul;  return ROP; }
^[ \t]?div   { yylval.val = Op_div;  return ROP; }
^[ \t]?and   { yylval.val = Op_and;  return ROP; }
^[ \t]?or    { yylval.val = Op_or;   return ROP; }
^[ \t]?xor   { yylval.val = Op_xor;  return ROP; }
^[ \t]?slt   { yylval.val = Op_slt;  return ROP; }
^[ \t]?shl   { yylval.val = Op_shl;  return ROP; }
^[ \t]?shr   { yylval.val = Op_shr;  return ROP; }
^[ \t]?addi  { yylval.val = Op_addi; return IOP; }
^[ \t]?andi  { yylval.val = Op_andi; return IOP; }
^[ \t]?ori   { yylval.val = Op_ori;  return IOP; }
^[ \t]?slti  { yylval.val = Op_slti; return IOP; }
^[ \t]?loi   { yylval.val = Op_loi;  return IOP; }
^[ \t]?hii   { yylval.val = Op_hii;  return IOP; }
^[ \t]?lw	 { yylval.val = Op_lw;   return IOP; }
^[ \t]?sw	 { yylval.val = Op_sw;   return IOP; }
^[ \t]?beq   { yylval.val = Op_beq;  return BOP; }
^[ \t]?bne   { yylval.val = Op_bne;  return BOP; }
^[ \t]?j	 { yylval.val = Op_j;    return JOP; }
^[ \t]?jal   { yylval.val = Op_jal;  return JOP; }
^[ \t]?jr	 { yylval.val = Op_jr;   return JOP; }
^[ \t]?jrl	 { yylval.val = Op_jrl;  return JOP; }
^[ \t]?addf  { yylval.val = Op_addf; return ROPF; }
^[ \t]?subf  { yylval.val = Op_subf; return ROPF; }
^[ \t]?mulf  { yylval.val = Op_mulf; return ROPF; }
^[ \t]?divf  { yylval.val = Op_divf; return ROPF; }
^[ \t]?sltf  { yylval.val = Op_sltf; return ROPF; }
^[ \t]?lof   { yylval.val = Op_lof;  return IOPF; }
^[ \t]?hif   { yylval.val = Op_hif;  return IOPF; }
^[ \t]?lwf   { yylval.val = Op_lwf;  return MOPF; }
^[ \t]?swf   { yylval.val = Op_swf;  return MOPF; }
^[ \t]?beqf  { yylval.val = Op_beqf; return BOPF; }
^[ \t]?bnef  { yylval.val = Op_bnef; return BOPF; }
^[ \t]?mod   { yylval.val = Op_mod;  return SOP; }
^[ \t]?crv   { yylval.val = Op_crv;  return SOP; }
^[ \t]?exp   { yylval.val = Op_exp;  return SOP; }
^[ \t]?mac   { yylval.val = Op_mac;  return SOP; }
^[ \t]?cas   { yylval.val = Op_cas;  return SOP; }
^[ \t]?brk	 { yylval.val = Op_brk;  return ROP; }
^[ \t]?halt	 { yylval.val = Op_halt; return ROP; }

 /* Integer Registers. */
[ \t]?r0   { yylval.val = Reg_r0; return INTREG; }
[ \t]?at   { yylval.val = Reg_at; return INTREG; }
[ \t]?v0   { yylval.val = Reg_v0; return INTREG; }
[ \t]?v1   { yylval.val = Reg_v1; return INTREG; }
[ \t]?a0   { yylval.val = Reg_a0; return INTREG; }
[ \t]?a1   { yylval.val = Reg_a1; return INTREG; }
[ \t]?a2   { yylval.val = Reg_a2; return INTREG; }
[ \t]?a3   { yylval.val = Reg_a3; return INTREG; }
[ \t]?t0   { yylval.val = Reg_t0; return INTREG; }
[ \t]?t1   { yylval.val = Reg_t1; return INTREG; }
[ \t]?t2   { yylval.val = Reg_t2; return INTREG; }
[ \t]?t3   { yylval.val = Reg_t3; return INTREG; }
[ \t]?t4   { yylval.val = Reg_t4; return INTREG; }
[ \t]?t5   { yylval.val = Reg_t5; return INTREG; }
[ \t]?t6   { yylval.val = Reg_t6; return INTREG; }
[ \t]?t7   { yylval.val = Reg_t7; return INTREG; }
[ \t]?t8   { yylval.val = Reg_t8; return INTREG; }
[ \t]?t9   { yylval.val = Reg_t9; return INTREG; }
[ \t]?s0   { yylval.val = Reg_s0; return INTREG; }
[ \t]?s1   { yylval.val = Reg_s1; return INTREG; }
[ \t]?s2   { yylval.val = Reg_s2; return INTREG; }
[ \t]?s3   { yylval.val = Reg_s3; return INTREG; }
[ \t]?s4   { yylval.val = Reg_s4; return INTREG; }
[ \t]?s5   { yylval.val = Reg_s5; return INTREG; }
[ \t]?s6   { yylval.val = Reg_s6; return INTREG; }
[ \t]?s7   { yylval.val = Reg_s7; return INTREG; }
[ \t]?k0   { yylval.val = Reg_k0; return INTREG; }
[ \t]?k1   { yylval.val = Reg_k1; return INTREG; }
[ \t]?gp   { yylval.val = Reg_gp; return INTREG; }
[ \t]?sp   { yylval.val = Reg_sp; return INTREG; }
[ \t]?fp   { yylval.val = Reg_fp; return INTREG; }
[ \t]?ra   { yylval.val = Reg_ra; return INTREG; }

 /* Floating Point Registers. */
[ \t]?fr0   { yylval.val = Reg_fr0;  return FPREG; }
[ \t]?fat   { yylval.val = Reg_fat;  return FPREG; }
[ \t]?fv0   { yylval.val = Reg_fv0;  return FPREG; }
[ \t]?fv1   { yylval.val = Reg_fv1;  return FPREG; }
[ \t]?fa0   { yylval.val = Reg_fa0;  return FPREG; }
[ \t]?fa1   { yylval.val = Reg_fa1;  return FPREG; }
[ \t]?fa2   { yylval.val = Reg_fa2;  return FPREG; }
[ \t]?fa3   { yylval.val = Reg_fa3;  return FPREG; }
[ \t]?ft0   { yylval.val = Reg_ft0;  return FPREG; }
[ \t]?ft1   { yylval.val = Reg_ft1;  return FPREG; }
[ \t]?ft2   { yylval.val = Reg_ft2;  return FPREG; }
[ \t]?ft3   { yylval.val = Reg_ft3;  return FPREG; }
[ \t]?ft4   { yylval.val = Reg_ft4;  return FPREG; }
[ \t]?ft5   { yylval.val = Reg_ft5;  return FPREG; }
[ \t]?ft6   { yylval.val = Reg_ft6;  return FPREG; }
[ \t]?ft7   { yylval.val = Reg_ft7;  return FPREG; }
[ \t]?ft8   { yylval.val = Reg_ft8;  return FPREG; }
[ \t]?ft9   { yylval.val = Reg_ft9;  return FPREG; }
[ \t]?ft10  { yylval.val = Reg_ft10; return FPREG; }
[ \t]?ft11  { yylval.val = Reg_ft11; return FPREG; }
[ \t]?fs0   { yylval.val = Reg_fs0;  return FPREG; }
[ \t]?fs1   { yylval.val = Reg_fs1;  return FPREG; }
[ \t]?fs2   { yylval.val = Reg_fs2;  return FPREG; }
[ \t]?fs3   { yylval.val = Reg_fs3;  return FPREG; }
[ \t]?fs4   { yylval.val = Reg_fs4;  return FPREG; }
[ \t]?fs5   { yylval.val = Reg_fs5;  return FPREG; }
[ \t]?fs6   { yylval.val = Reg_fs6;  return FPREG; }
[ \t]?fs7   { yylval.val = Reg_fs7;  return FPREG; }
[ \t]?fs8   { yylval.val = Reg_fs8;  return FPREG; }
[ \t]?fs9   { yylval.val = Reg_fs9;  return FPREG; }
[ \t]?fs10  { yylval.val = Reg_fs10; return FPREG; }
[ \t]?fs11  { yylval.val = Reg_fs11; return FPREG; }

 /* Assembler Directives. */
[ \t]?.text  { yylval.val = Directive_Text; return DIRECTIVE; }
[ \t]?.data  { yylval.val = Directive_Data; return DIRECTIVE; }
[ \t]?.datum { yylval.val = Directive_Datum; return DIRECTIVE; }

 /* A number */
[-+]?[0-9]+	{ yylval.num = atoi(yytext); return NUM; }

 /* A hexadecimal number */
[-+]?0[xX][0-9a-fA-F]+ { yylval.num = strtol(yytext, NULL, 0); return NUM; }

 /* A label */
[a-zA-Z][a-zA-Z0-9_]*:  { yylval.label = yytext; return LABEL; }

\n          { return EOL; }
"#".*       /* A comment */
[ \t]       /* ignore white space */
.           { yyerror(yylineno); }

%%
